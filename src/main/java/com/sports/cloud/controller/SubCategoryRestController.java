package main.java.com.sports.cloud.controller;

import java.util.List;

import main.java.com.sports.cloud.model.SubCategory;
import main.java.com.sports.cloud.service.SubCategoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class SubCategoryRestController {
	
	@Autowired
	SubCategoryService subCategoryService;
	
	
 
    @RequestMapping(value="/createSubCategory", method=RequestMethod.POST)
    public ResponseEntity<SubCategory> createSubCategory(@RequestBody SubCategory subCategory) {
    	subCategoryService.createSubCategory(subCategory);
    	 return new ResponseEntity<SubCategory>(subCategory, HttpStatus.OK);
    }
    
 
    
    @RequestMapping(value="/deleteSubCategory", method=RequestMethod.DELETE)
    public ResponseEntity<SubCategory> deleteSubCategory(@RequestBody SubCategory subCategory) {
    	subCategoryService.deleteSubCategory(subCategory);
    	 return new ResponseEntity<SubCategory>(subCategory, HttpStatus.OK);
    }
    
    @RequestMapping(value="/listAllSubCategory", method=RequestMethod.GET)
    public  ResponseEntity<List<SubCategory>> getAllSubCategoryList() {
   
    	return new ResponseEntity<List<SubCategory>>(subCategoryService.ListAllSubCategory(), HttpStatus.OK);
        
    }
    
    @RequestMapping(value="/listSubCategoryOnCatId/{categoryId}", method=RequestMethod.GET)
    public  ResponseEntity<List<SubCategory>> getSubCategoryList(@PathVariable String categoryId) {
    	//System.out.println(categoryId);
    	
    	return new ResponseEntity<List<SubCategory>>(subCategoryService.ListSubCategoryOnCategoryId((long)Integer.parseInt(categoryId)), HttpStatus.OK);
        
    }
    
   /* @RequestMapping(value="/listOnCategoryId", method=RequestMethod.GET)
    public  ResponseEntity<List<SubCategory>> listOnCategoryId() {
   
    	return new ResponseEntity<List<SubCategory>>(subCategoryService.findAllSubCategory(), HttpStatus.OK);
        
    }*/
    
   /* @RequestMapping(value="/findSubCategory/{SubCategory}", method=RequestMethod.GET)
    public  List<SubCategory> getSubCategoryList(@PathVariable SubCategory subCategory) {
    	
    	//TODO Update for all lists on the basis of filled value
        return subCategoryService.findSubCategoryOnCategoryId(subCategory);
    }*/
    
//    @RequestMapping(value = "/hello", method = RequestMethod.GET)
//    public ResponseEntity<List<User>> allUsers() {
//    	return new ResponseEntity<List<User>>(userService.findAllUsers(), HttpStatus.OK);
//    }
    /*
    @RequestMapping(value = "/hello", method = RequestMethod.POST)
    public ResponseEntity<User> update(@RequestBody User user) {
        if (user != null) {
            user.setUsername(user.getUsername() + " - new");
        }
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }*/
 
}
