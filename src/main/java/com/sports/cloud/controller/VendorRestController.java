package main.java.com.sports.cloud.controller;

import java.util.List;

import main.java.com.sports.cloud.model.Vendor;
import main.java.com.sports.cloud.service.VendorService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VendorRestController {

		@Autowired
		VendorService vendorService;
		
		@RequestMapping(value="/createVendor", method=RequestMethod.POST)
	    public ResponseEntity<Vendor> createVendor(@RequestBody Vendor vendor) {
			vendorService.createVendor(vendor);
	        return new ResponseEntity<Vendor>(vendor, HttpStatus.OK);

	    }

		 @RequestMapping(value="/listAllVendor", method=RequestMethod.GET)
		    public  ResponseEntity< List<Vendor>> getAllVendorList() {
		     return new ResponseEntity< List<Vendor>>(vendorService.ListAllVendor(), HttpStatus.OK);
		    }
		 
		 @RequestMapping(value="/deleteVendor", method=RequestMethod.DELETE)
		    public ResponseEntity<Vendor> deleteVendor(@RequestBody Vendor vendor) {
				vendorService.deleteVendor(vendor);
		        return new ResponseEntity<Vendor>(vendor, HttpStatus.OK);

		    }
}
