package main.java.com.sports.cloud.controller;



import java.util.List;

import main.java.com.sports.cloud.model.User;
import main.java.com.sports.cloud.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


 
@RestController
public class UserRestController {
	
	@Autowired
	UserService userService;
	
	@RequestMapping("/")
	public String greetUser() {
		return "REST API SERVER - SPORTS CLUB";
	}
 
	
	 @RequestMapping(value="/createUser", method=RequestMethod.POST)
	    public ResponseEntity<User> createUser(@RequestBody User user) {
		 userService.createUser(user);
	        return new ResponseEntity<User>(user, HttpStatus.OK);

	    }
	 
	 @RequestMapping(value="/listAllUser", method=RequestMethod.GET)
	    public  ResponseEntity< List<User>> getAllUserList() {
	     return new ResponseEntity< List<User>>(userService.ListAllUsers(), HttpStatus.OK);
	    }

	 
	 @RequestMapping(value="/deleteUser", method=RequestMethod.DELETE)
	    public ResponseEntity<User> deleteUser(@RequestBody User user) {
		 userService.deleteUser(user);
	        return new ResponseEntity<User>(user, HttpStatus.OK);

	    }

	 
   /* @RequestMapping("/hello/{name}")
    public User user(@PathVariable String name) {
        User user = new User();
        user.setUsername(name);
        userService.saveUser(user);
        return user;
    }
    
    @RequestMapping("/hello")
    public User getuser(@RequestParam(value = "name", defaultValue = "dummy") String name) {
        User user = new User();
        user.setUsername(name);
        return user;
    }*/
    
//    @RequestMapping(value = "/hello", method = RequestMethod.GET)
//    public ResponseEntity<List<User>> allUsers() {
//    	return new ResponseEntity<List<User>>(userService.findAllUsers(), HttpStatus.OK);
//    }
    
/*    @RequestMapping(value = "/hello", method = RequestMethod.POST)
    public ResponseEntity<User> update(@RequestBody User user) {
        if (user != null) {
            user.setUsername(user.getUsername() + " - new");
        }
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }
 */
}