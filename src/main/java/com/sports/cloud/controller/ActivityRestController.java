package main.java.com.sports.cloud.controller;

import java.util.List;

import main.java.com.sports.cloud.model.Activity;
import main.java.com.sports.cloud.service.ActivityService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ActivityRestController {
	
	@Autowired
	ActivityService activityService;
	
	
	 @RequestMapping(value="/createActivity", method=RequestMethod.POST)
	    public ResponseEntity<Activity> createActvity(@RequestBody Activity activity) {
		 activityService.createActivity(activity);
	        return new ResponseEntity<Activity>(activity, HttpStatus.OK);

	    }
  
	 @RequestMapping(value="/listAllActivity", method=RequestMethod.GET)
	    public  ResponseEntity< List<Activity>> getAllActivityList() {
	       return new ResponseEntity< List<Activity>>(activityService.ListAllActivity(), HttpStatus.OK);
	    }
	 
	 @RequestMapping(value="/deleteActivity", method=RequestMethod.DELETE)
	    public  ResponseEntity<Activity> deleteActivity(@RequestBody Activity activity) {
		 activityService.deleteActivity(activity);
	        return new ResponseEntity<Activity>(activity, HttpStatus.OK);
	    }
}