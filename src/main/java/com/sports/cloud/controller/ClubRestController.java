package main.java.com.sports.cloud.controller;

import java.util.List;

import main.java.com.sports.cloud.model.Club;
import main.java.com.sports.cloud.service.ClubService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ClubRestController {
	
	@Autowired
	ClubService clubService;
	
	 @RequestMapping(value="/createClub", method=RequestMethod.POST)
	    public ResponseEntity<Club> createClub(@RequestBody Club club) {
		 clubService.createClub(club);
	        return new ResponseEntity<Club>(club, HttpStatus.OK);

	    }
	
	 @RequestMapping(value="/listAllClub", method=RequestMethod.GET)
	    public  ResponseEntity< List<Club>> getClubList() {
	     return new ResponseEntity< List<Club>>(clubService.ListAllClubs(), HttpStatus.OK);
	    }
  
	 @RequestMapping(value="/deleteClub", method=RequestMethod.DELETE)
	    public  ResponseEntity<Club> deleteActivity(@RequestBody Club club) {
		 clubService.deleteClub(club);
	        return new ResponseEntity<Club>(club, HttpStatus.OK);
	    }
	 
	 @RequestMapping(value="/fetchClubList/{activityId}/{clubType}/{clubFlg}/{bookingDate}/{startTime}/{endTime}", method=RequestMethod.GET)
	    public  ResponseEntity<String> fetchClubList(@PathVariable String activityId, @PathVariable String clubType,@PathVariable String clubFlg,@PathVariable String bookingDate,
	    		@PathVariable String startTime,@PathVariable String endTime) {
		 System.out.println(activityId);
		 System.out.println(clubType);
		 System.out.println(clubFlg);
		 System.out.println(bookingDate);
		 System.out.println(startTime);
		 System.out.println(endTime);
		
		 clubService.fetchClubList(activityId, clubType, clubFlg, bookingDate, startTime, endTime);
		 
	        return new ResponseEntity<String>("Done", HttpStatus.OK);
	    }
}
