package main.java.com.sports.cloud.controller;

import java.util.List;

import main.java.com.sports.cloud.model.FavClub;
import main.java.com.sports.cloud.service.FavClubService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class FavClubRestController {
	
	@Autowired
	FavClubService favClubService;
	
	 @RequestMapping(value="/createFavClub", method=RequestMethod.POST)
	    public ResponseEntity<FavClub> createFavClub(@RequestBody FavClub favClub) {
		 favClubService.createFavClub(favClub);
	        return new ResponseEntity<FavClub>(favClub, HttpStatus.OK);

	    }
 
	 @RequestMapping(value="/listAllFavClub", method=RequestMethod.GET)
	    public  ResponseEntity< List<FavClub>> getFavClubList() {
	    	
	       
	        return new ResponseEntity< List<FavClub>>(favClubService.ListAllFavClub(), HttpStatus.OK);
	    }
  
	 
	 @RequestMapping(value="/deleteFavClub", method=RequestMethod.DELETE)
	    public ResponseEntity<FavClub> deleteFavClub(@RequestBody FavClub favClub) {
		 favClubService.deleteFavClub(favClub);
	        return new ResponseEntity<FavClub>(favClub, HttpStatus.OK);

	    }
	 
}