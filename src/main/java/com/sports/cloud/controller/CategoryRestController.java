package main.java.com.sports.cloud.controller;

import java.util.List;

import main.java.com.sports.cloud.model.Category;
import main.java.com.sports.cloud.service.CategoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class CategoryRestController {
	
	@Autowired
	CategoryService categoryService;
	
	
 
    @RequestMapping(value="/createCategory", method=RequestMethod.POST)
    public ResponseEntity<Category> createCategory(@RequestBody Category category) {
    	categoryService.createCategory(category);
        return new ResponseEntity<Category>(category, HttpStatus.OK);

    }
    
    
    @RequestMapping(value="/deleteCategory", method=RequestMethod.DELETE)
    public ResponseEntity<Category> deleteCategory(@RequestBody Category category) {
    	categoryService.deleteCategory(category);
        return new ResponseEntity<Category>(category, HttpStatus.OK);

    }
    
   /* @RequestMapping(value="/deleteCategoryByID/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<Category> deleteCategoryByID(@PathVariable Long id) {
    	 return new ResponseEntity<Category>(categoryService.deleteCategoryByID(id), HttpStatus.OK);
    }*/
    
 @RequestMapping(value="/listAllCategory", method=RequestMethod.GET)
    public  ResponseEntity< List<Category>> getCategoryList() {
    	 return new ResponseEntity< List<Category>>(categoryService.ListAllCategory(), HttpStatus.OK);
    }
    
   /* @RequestMapping(value="/ListCategory/{id}", method=RequestMethod.GET)
    public  ResponseEntity< List<Category>> getCategoryList(@PathVariable Long	Id) {
    	
       Category category=new Category();
       category.setId(Id);
        return new ResponseEntity< List<Category>>(categoryService.ListCategory(category), HttpStatus.OK);
    }*/
    
//    @RequestMapping(value = "/hello", method = RequestMethod.GET)
//    public ResponseEntity<List<User>> allUsers() {
//    	return new ResponseEntity<List<User>>(userService.findAllUsers(), HttpStatus.OK);
//    }
    /*
    @RequestMapping(value = "/hello", method = RequestMethod.POST)
    public ResponseEntity<User> update(@RequestBody User user) {
        if (user != null) {
            user.setUsername(user.getUsername() + " - new");
        }
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }*/
 
}
