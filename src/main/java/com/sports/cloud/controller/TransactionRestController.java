package main.java.com.sports.cloud.controller;


import java.util.List;

import main.java.com.sports.cloud.model.Transaction;
import main.java.com.sports.cloud.service.TransactionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TransactionRestController {

		@Autowired
		TransactionService transactionService;
	
		@RequestMapping(value="/createTransaction", method=RequestMethod.POST)
	    public ResponseEntity<Transaction> createSports(@RequestBody Transaction transaction) {
			transactionService.createTransaction(transaction);
	        return new ResponseEntity<Transaction>(transaction, HttpStatus.OK);

	    }
		
		 @RequestMapping(value="/listAllTransaction", method=RequestMethod.GET)
		    public  ResponseEntity< List<Transaction>> getAllTransactionList() {
		     return new ResponseEntity< List<Transaction>>(transactionService.ListAllTransactions(), HttpStatus.OK);
		    }
		 
		 
		 @RequestMapping(value="/deleteTransaction", method=RequestMethod.DELETE)
		    public ResponseEntity<Transaction> deleteSports(@RequestBody Transaction transaction) {
				transactionService.deleteTransaction(transaction);
		        return new ResponseEntity<Transaction>(transaction, HttpStatus.OK);

		    }
}
