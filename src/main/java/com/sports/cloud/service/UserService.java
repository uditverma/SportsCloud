package main.java.com.sports.cloud.service;



import java.util.List;

import main.java.com.sports.cloud.model.User;



public interface UserService {

   public User createUser(User user);
   public List<User> ListAllUsers();
   public User deleteUser(User user);
}