package main.java.com.sports.cloud.service;

import java.util.List;

import main.java.com.sports.cloud.model.Vendor;

public interface VendorService {

	public List<Vendor> ListAllVendor();
	public Vendor createVendor(Vendor vendor);
	public Vendor deleteVendor(Vendor vendor);
}
