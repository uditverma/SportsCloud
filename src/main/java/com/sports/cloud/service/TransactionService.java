package main.java.com.sports.cloud.service;

import java.util.List;

import main.java.com.sports.cloud.model.Transaction;

public interface TransactionService {

	public List<Transaction> ListAllTransactions();
	public Transaction createTransaction(Transaction transaction);
	public Transaction deleteTransaction(Transaction transaction);
}
