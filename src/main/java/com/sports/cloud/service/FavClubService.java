package main.java.com.sports.cloud.service;

import java.util.List;

import main.java.com.sports.cloud.model.FavClub;

public interface FavClubService {

	public List<FavClub> ListAllFavClub();
	public FavClub createFavClub(FavClub favClub);
	public FavClub deleteFavClub(FavClub favClub);
}
