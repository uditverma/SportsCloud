package main.java.com.sports.cloud.service;

import java.util.List;

import main.java.com.sports.cloud.model.Category;

public interface CategoryService {

	   public Category createCategory(Category category);
	   public List<Category> ListAllCategory();
	   public Category deleteCategory(Category category);
}
