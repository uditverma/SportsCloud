package main.java.com.sports.cloud.service;

import java.util.List;

import main.java.com.sports.cloud.model.SubCategory;

public interface SubCategoryService {
	
	public void createSubCategory(SubCategory subCategory);
	public List<SubCategory> ListAllSubCategory();
	public List<SubCategory> ListSubCategoryOnCategoryId(Long categoryId);
    public SubCategory deleteSubCategory(SubCategory subCategory);

}
