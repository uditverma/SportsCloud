package main.java.com.sports.cloud.service;

import java.util.List;

import main.java.com.sports.cloud.model.Activity;

public interface ActivityService {

	public List<Activity> ListAllActivity();
	public Activity createActivity(Activity activity);
	public Activity deleteActivity(Activity activity);
	public List<Activity> FetchActivity(Activity activity);
}
