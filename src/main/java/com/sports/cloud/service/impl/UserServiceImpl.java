package main.java.com.sports.cloud.service.impl;



import java.util.List;

import main.java.com.sports.cloud.dao.UserDAO;
import main.java.com.sports.cloud.model.User;
import main.java.com.sports.cloud.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


 
@Service("userService")
@Transactional
public class UserServiceImpl implements UserService{
 
    @Autowired
    private UserDAO userDao;
     
    public User createUser(User user) {
        userDao.saveUser(user);
        return user;
    }
 
    public List<User> ListAllUsers() {
        return userDao.ListAllUsers();
    }


	@Override
	public User deleteUser(User user) {
		 userDao.deleteUser(user);
	        return user;
	}
}