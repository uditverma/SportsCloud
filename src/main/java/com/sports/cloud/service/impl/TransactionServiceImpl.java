package main.java.com.sports.cloud.service.impl;

import java.util.List;

import main.java.com.sports.cloud.dao.TransactionDao;
import main.java.com.sports.cloud.model.Transaction;
import main.java.com.sports.cloud.service.TransactionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("TransactionService")
@Transactional
public class TransactionServiceImpl implements TransactionService {

	@Autowired
	TransactionDao	transactionDao;

	@Override
	public List<Transaction> ListAllTransactions() {

		return transactionDao.listAllTransactions();
	}

	@Override
	public Transaction createTransaction(Transaction transaction) {
		transactionDao.createTransaction(transaction);
		return transaction;
	}

	@Override
	public Transaction deleteTransaction(Transaction transaction) {
		transactionDao.deleteTransaction(transaction);
		return transaction;
	}

}
