package main.java.com.sports.cloud.service.impl;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

import javax.transaction.Transactional;

import main.java.com.sports.cloud.clubEngine.EngineManager;
import main.java.com.sports.cloud.clubEngine.Criteria;
import main.java.com.sports.cloud.dao.ClubDao;
import main.java.com.sports.cloud.model.Club;
import main.java.com.sports.cloud.service.ClubService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("clubService")
@Transactional
public class ClubServiceImpl implements ClubService {

		@Autowired
	    private ClubDao clubDao;

		@Override
		public List<Club> ListAllClubs() {
			return clubDao.listAllClub();
		}

		@Override
		public Club createClub(Club club) {
			clubDao.createClub(club);
			return club;
		}

		@Override
		public Club deleteClub(Club club) {
			clubDao.deleteClub(club);
			return club;
		}
		
		@Override
		public Club fetchClubList(String activityId,String clubType,String clubFlg,String bookingDate,String startTime,String endTime) {
			//clubDao.deleteClub(club);
			
			Criteria Crit=new Criteria(Date.valueOf(bookingDate), Time.valueOf(startTime),
					Time.valueOf(endTime), Integer.parseInt(clubType), 
					Integer.parseInt(clubFlg), activityId);
			System.out.println(Crit.toString());
			EngineManager engineManager=new EngineManager(Crit);
			engineManager.fiterData();
			
			return null;
		}
		
}
