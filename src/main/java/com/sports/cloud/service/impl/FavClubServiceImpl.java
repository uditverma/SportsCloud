package main.java.com.sports.cloud.service.impl;

import java.util.List;

import main.java.com.sports.cloud.dao.FavClubDao;
import main.java.com.sports.cloud.model.FavClub;
import main.java.com.sports.cloud.service.FavClubService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("favClubService")
@Transactional
public class FavClubServiceImpl implements FavClubService{

	@Autowired
	FavClubDao	favClubDao;

	@Override
	public List<FavClub> ListAllFavClub() {
		
		return favClubDao.listAllFavClub();
	}

	@Override
	public FavClub createFavClub(FavClub favClub) {
		favClubDao.createFavClub(favClub);
		return favClub;
	}

	@Override
	public FavClub deleteFavClub(FavClub favClub) {
		favClubDao.deleteFavClub(favClub);
		return favClub;
	}
	
	
}
