package main.java.com.sports.cloud.service.impl;

import java.util.List;

import main.java.com.sports.cloud.dao.ActivityDao;
import main.java.com.sports.cloud.model.Activity;
import main.java.com.sports.cloud.service.ActivityService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("activityService")
@Transactional
public class ActivityServiceImpl implements ActivityService {

	@Autowired
	ActivityDao	ActivityDao;

	@Override
	public List<Activity> ListAllActivity() {

		return ActivityDao.listAllActivity();
	}

	@Override
	public Activity createActivity(Activity activity) {
		ActivityDao.createActivity(activity);
		return activity;
	}

	@Override
	public Activity deleteActivity(Activity activity) {
		ActivityDao.deleteActivity(activity);
		return activity;
	}

	

	@Override
	public List<Activity> FetchActivity(Activity activity) 
	{
		/*Activity temp=new Activity();
		temp.setName(Name);*/
		return ActivityDao.fetchActivity(activity);
		
	}
}
