package main.java.com.sports.cloud.service.impl;



import java.util.List;

import main.java.com.sports.cloud.config.SubCategoryFilter;
import main.java.com.sports.cloud.dao.SubCategoryDao;
import main.java.com.sports.cloud.model.SubCategory;
import main.java.com.sports.cloud.service.SubCategoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


 
@Service("SubCategoryService")
@Transactional
public class SubCategoryServiceImpl implements SubCategoryService{

	@Autowired
    private SubCategoryDao subCategoryDao;
	


	@Override
	public List<SubCategory> ListAllSubCategory() {
		return subCategoryDao.ListAllSubCategory();
	}

	

	@Override
	public void createSubCategory(SubCategory subCategory) {
		subCategoryDao.saveSubCategory(subCategory);
		
	}

	

	@Override
	public SubCategory deleteSubCategory(SubCategory subCategory) {
		subCategoryDao.deleteSubCategory(subCategory);
		return subCategory;
	}



	@Override
	public List<SubCategory> ListSubCategoryOnCategoryId(Long categoryId) {
		SubCategory temp=new SubCategory();
		temp.setCategoryId(categoryId);
		/*
		System.out.println("service: "+categoryId);
		
		
		*/
		return subCategoryDao.findSubCategory(temp, SubCategoryFilter.CategoryId);
	}



	

	
}