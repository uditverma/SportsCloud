package main.java.com.sports.cloud.service.impl;



import java.util.List;

import main.java.com.sports.cloud.dao.CategoryDao;
import main.java.com.sports.cloud.model.Category;
import main.java.com.sports.cloud.service.CategoryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


 
@Service("categoryService")
@Transactional
public class CategoryServiceImpl implements CategoryService{

	@Autowired
    private CategoryDao categoryDao;
	
	@Override
	public Category createCategory(Category category) {
		categoryDao.saveCategory(category);
		return category;
		
	}

	/*@Override
	public List<Category> findAllCategory(Category category) {
		return categoryDao.findAllCategory(Category category);
	}*/

	@Override
	public Category deleteCategory(Category category) {
		categoryDao.deleteCategory(category);
		return category;
		
	}

	@Override
	public List<Category> ListAllCategory() {
		return categoryDao.findAllCategory();
		
	}

	

	
 

	
}