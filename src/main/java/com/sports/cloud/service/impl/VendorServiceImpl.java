package main.java.com.sports.cloud.service.impl;

import java.util.List;

import main.java.com.sports.cloud.dao.VendorDao;
import main.java.com.sports.cloud.model.Vendor;
import main.java.com.sports.cloud.service.VendorService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("VendorService")
@Transactional
public class VendorServiceImpl implements VendorService {

	@Autowired
	VendorDao vendorDao;
	
	@Override
	public List<Vendor> ListAllVendor() {
		return vendorDao.listAllVendors();
	}

	@Override
	public Vendor createVendor(Vendor vendor) {
		vendorDao.createVendor(vendor);
		return vendor;
	}

	@Override
	public Vendor deleteVendor(Vendor vendor) {
		vendorDao.deleteVendor(vendor);
		return vendor;
	}

}
