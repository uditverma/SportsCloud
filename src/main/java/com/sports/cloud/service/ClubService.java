package main.java.com.sports.cloud.service;

import java.util.List;

import main.java.com.sports.cloud.model.Club;

public interface ClubService {

	   public List<Club> ListAllClubs();
	   public Club createClub(Club club);
	   public Club deleteClub(Club club);
	Club fetchClubList(String activityId, String clubType, String clubFlg,
			String bookingDate, String startTime, String endTime);
}
