package main.java.com.sports.cloud.config;



import java.util.Properties;

import javax.sql.DataSource;

import main.java.com.sports.cloud.dao.CategoryDao;
import main.java.com.sports.cloud.dao.ClubDao;
import main.java.com.sports.cloud.dao.FavClubDao;
import main.java.com.sports.cloud.dao.ActivityDao;
import main.java.com.sports.cloud.dao.SubCategoryDao;
import main.java.com.sports.cloud.dao.TransactionDao;
import main.java.com.sports.cloud.dao.UserDAO;
import main.java.com.sports.cloud.dao.VendorDao;
import main.java.com.sports.cloud.dao.impl.CategoryDaoImpl;
import main.java.com.sports.cloud.dao.impl.ClubDaoImpl;
import main.java.com.sports.cloud.dao.impl.FavClubDaoImpl;
import main.java.com.sports.cloud.dao.impl.ActivityDaoImpl;
import main.java.com.sports.cloud.dao.impl.SubCategoryDaoImpl;
import main.java.com.sports.cloud.dao.impl.TransactionDaoImpl;
import main.java.com.sports.cloud.dao.impl.UserDAOImpl;
import main.java.com.sports.cloud.dao.impl.VendorDaoImpl;
import main.java.com.sports.cloud.service.CategoryService;
import main.java.com.sports.cloud.service.ClubService;
import main.java.com.sports.cloud.service.FavClubService;
import main.java.com.sports.cloud.service.ActivityService;
import main.java.com.sports.cloud.service.SubCategoryService;
import main.java.com.sports.cloud.service.TransactionService;
import main.java.com.sports.cloud.service.UserService;
import main.java.com.sports.cloud.service.VendorService;
import main.java.com.sports.cloud.service.impl.CategoryServiceImpl;
import main.java.com.sports.cloud.service.impl.ClubServiceImpl;
import main.java.com.sports.cloud.service.impl.FavClubServiceImpl;
import main.java.com.sports.cloud.service.impl.ActivityServiceImpl;
import main.java.com.sports.cloud.service.impl.SubCategoryServiceImpl;
import main.java.com.sports.cloud.service.impl.TransactionServiceImpl;
import main.java.com.sports.cloud.service.impl.UserServiceImpl;
import main.java.com.sports.cloud.service.impl.VendorServiceImpl;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;



@Configuration
@EnableWebMvc
@EnableTransactionManagement
@ComponentScan(basePackages = "main.java.com.sports.cloud")
@PropertySource(value = { "classpath:application.properties" })
public class AppConfig {

	@Autowired
	private Environment environment;

	@Bean(name = "dataSource")
	public DataSource getDataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName(environment.getRequiredProperty("jdbc.driverClassName"));
		dataSource.setUrl(environment.getRequiredProperty("jdbc.url"));
		dataSource.setUsername(environment.getRequiredProperty("jdbc.username"));
		dataSource.setPassword(environment.getRequiredProperty("jdbc.password"));
		return dataSource;
	}

	@Autowired
	@Bean(name = "sessionFactory")
	public SessionFactory getSessionFactory(DataSource dataSource) {
		LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(dataSource);
		sessionBuilder.scanPackages("main.java.com.sports.cloud.model");
		sessionBuilder.addProperties(getHibernateProperties());
		return sessionBuilder.buildSessionFactory();
	}

	@Autowired
	@Bean(name = "transactionManager")
	public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {
		HibernateTransactionManager transactionManager = new HibernateTransactionManager(sessionFactory);
		return transactionManager;
	}

	@Autowired
	@Bean(name = "userDao")
	public UserDAO getUserDao(SessionFactory sessionFactory) {
		return new UserDAOImpl(sessionFactory);
	}
	
	@Autowired
	@Bean(name = "categoryDao")
	public CategoryDao getCategoryDao(SessionFactory sessionFactory) {
		return new CategoryDaoImpl(sessionFactory);
	}

	@Autowired
	@Bean(name = "userService")
	public UserService getUserService() {
		return new UserServiceImpl();
	}
	
	@Autowired
	@Bean(name = "categoryService")
	public CategoryService getCategoryService() {
		return new CategoryServiceImpl();
	}
	
	@Autowired
	@Bean(name = "ClubDao")
	public ClubDao getClubDao(SessionFactory sessionFactory) {
		return new ClubDaoImpl(sessionFactory);
	}
	
	@Autowired
	@Bean(name = "ClubServiceService")
	public ClubService getClubService() {
		return new ClubServiceImpl();
	}

	
	@Autowired
	@Bean(name = "VendorDao")
	public VendorDao getVendorDao(SessionFactory sessionFactory) {
		return new VendorDaoImpl(sessionFactory);
	}
	
	@Autowired
	@Bean(name = "VendorService")
	public VendorService getVendorService() {
		return new VendorServiceImpl();
	}
	
	@Autowired
	@Bean(name = "SubCategoryDao")
	public SubCategoryDao getSubCategoryDao(SessionFactory sessionFactory) {
		return new SubCategoryDaoImpl(sessionFactory);
	}
	
	@Autowired
	@Bean(name = "SubCategoryService")
	public SubCategoryService getSubCategoryService() {
		return new SubCategoryServiceImpl();
	}
	
	@Autowired
	@Bean(name = "FavCLubDao")
	public FavClubDao getFavClubDao(SessionFactory sessionFactory) {
		return new FavClubDaoImpl(sessionFactory);
	}
	
	@Autowired
	@Bean(name = "FavClubService")
	public FavClubService getFavClubService() {
		return new FavClubServiceImpl();
	}
	
	@Autowired
	@Bean(name = "SportsDao")
	public ActivityDao getSportsDao(SessionFactory sessionFactory) {
		return new ActivityDaoImpl(sessionFactory);
	}
	
	@Autowired
	@Bean(name = "SportsService")
	public ActivityService getSportsService() {
		return new ActivityServiceImpl();
	}
	
	
	@Autowired
	@Bean(name = "TransactionDao")
	public TransactionDao getTransactionDao(SessionFactory sessionFactory) {
		return new TransactionDaoImpl(sessionFactory);
	}
	
	@Autowired
	@Bean(name = "TransactionService")
	public TransactionService getTransactionService() {
		return new TransactionServiceImpl();
	}
	
	
	private Properties getHibernateProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.dialect", environment.getRequiredProperty("hibernate.dialect"));
		properties.put("hibernate.show_sql", environment.getRequiredProperty("hibernate.show_sql"));
		properties.put("hibernate.format_sql", environment.getRequiredProperty("hibernate.format_sql"));
		properties.put("hibernate.hbm2ddl.auto", environment.getRequiredProperty("hibernate.hbm2ddl.auto"));
		return properties;
	}

}