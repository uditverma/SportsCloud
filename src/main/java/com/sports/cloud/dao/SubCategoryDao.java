package main.java.com.sports.cloud.dao;

import java.util.List;

import main.java.com.sports.cloud.config.SubCategoryFilter;
import main.java.com.sports.cloud.model.SubCategory;

public interface SubCategoryDao {
	public List<SubCategory> ListAllSubCategory();
	public List<SubCategory> findSubCategory(SubCategory subCategory, Enum<SubCategoryFilter> filter);
    public void saveSubCategory(SubCategory subCategory);
    public void deleteSubCategory(SubCategory subCategory);
}
