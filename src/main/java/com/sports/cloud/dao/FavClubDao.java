/**
 * 
 */
package main.java.com.sports.cloud.dao;

import java.util.List;

import main.java.com.sports.cloud.model.FavClub;

/**
 * @author DK
 *
 */
public interface FavClubDao {

	public List<FavClub> listAllFavClub();
	public void createFavClub(FavClub favclub);
	public void deleteFavClub(FavClub favclub);
}
