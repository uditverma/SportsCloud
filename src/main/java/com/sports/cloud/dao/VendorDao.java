package main.java.com.sports.cloud.dao;

import java.util.List;

import main.java.com.sports.cloud.model.Vendor;

public interface VendorDao {
	
	public List<Vendor> listAllVendors();
	public List<Vendor> listVendorsOnCrit(Vendor vendor);
    public void createVendor(Vendor vendor);
    public void deleteVendor(Vendor vendor);
    public void modifyVendor(Vendor vendor);

}
