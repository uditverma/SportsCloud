/**
 * 
 */
package main.java.com.sports.cloud.dao;

import java.util.List;

import main.java.com.sports.cloud.model.Activity;

/**
 * @author DK
 *
 */
public interface ActivityDao {
	
	public List<Activity> listAllActivity();
	public void createActivity(Activity activity);
	public List<Activity> fetchActivity(Activity activity);
	public void deleteActivity(Activity activity);
}
