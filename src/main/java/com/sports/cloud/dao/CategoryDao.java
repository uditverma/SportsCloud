package main.java.com.sports.cloud.dao;

import java.util.List;

import main.java.com.sports.cloud.model.Category;



public interface CategoryDao {
	public List<Category> findAllCategory();
    public void saveCategory(Category category);
    public void deleteCategory(Category category);
  

}
