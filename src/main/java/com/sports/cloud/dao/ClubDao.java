/**
 * 
 */
package main.java.com.sports.cloud.dao;

import java.util.List;

import main.java.com.sports.cloud.model.Club;


/**
 * @author DK
 *
 */
public interface ClubDao {

	public List<Club> listAllClub();
	public void createClub(Club club);
	public void deleteClub(Club club);
	
   
}
