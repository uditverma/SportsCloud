/**
 * 
 */
package main.java.com.sports.cloud.dao;

import java.util.List;

import main.java.com.sports.cloud.model.Transaction;

/**
 * @author DK
 *
 */
public interface TransactionDao {
	
	public List<Transaction> listAllTransactions();
	public void createTransaction(Transaction transaction);
	public void deleteTransaction(Transaction transaction);
}
