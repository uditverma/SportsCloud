package main.java.com.sports.cloud.dao.impl;

import java.util.List;

import javax.transaction.Transactional;

import main.java.com.sports.cloud.dao.AbstractDao;
import main.java.com.sports.cloud.dao.ActivityDao;
import main.java.com.sports.cloud.model.Activity;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

public class ActivityDaoImpl extends AbstractDao implements ActivityDao {

	public ActivityDaoImpl() {
		super();
	}

	public ActivityDaoImpl(SessionFactory sessionFactory) {
		super(sessionFactory);

	}

	@SuppressWarnings("unchecked")
	@Override
    @Transactional
	public List<Activity> listAllActivity() {
		Criteria criteria = getSession().createCriteria(Activity.class);
		return (List<Activity>) criteria.list();

	}

	@Override
	public void createActivity(Activity activity) {
		persist(activity);
		
	}

	@Override
	public void deleteActivity(Activity activity) {
		delete(activity);
		
	}

	@Override
	public List<Activity> fetchActivity(Activity activity) {
		Criteria criteria = getSession().createCriteria(Activity.class);
		criteria.add(Restrictions.eq("blockFlg","0"));
		if(activity!=null)
		{
			if(activity.getName()!=null)
				criteria.add(Restrictions.eq("name",activity.getName()));
			if(activity.getPnpMode()==1)
				criteria.add(Restrictions.eq("pnpMode","1"));
			if(activity.getSubMode()==1)
				criteria.add(Restrictions.eq("subMode","1"));
			
				
		}
		return (List<Activity>) criteria.list();
	}



}
