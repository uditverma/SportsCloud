package main.java.com.sports.cloud.dao.impl;

import java.util.List;

import javax.transaction.Transactional;

import main.java.com.sports.cloud.dao.AbstractDao;
import main.java.com.sports.cloud.dao.VendorDao;
import main.java.com.sports.cloud.model.Vendor;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;



@Repository("VendorDao")
public class VendorDaoImpl extends AbstractDao implements VendorDao {
 
	
	public VendorDaoImpl() {
		super();
	}
	
	public VendorDaoImpl(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

/*	@Override
	public List<Vendor> listAllVendors(Vendor vendor) {
		 Criteria criteria = getSession().createCriteria(Category.class);
		if(vendor.getEmail().)
			
			
		else if()
		return null;
	}*/

	@Override
	public void createVendor(Vendor vendor) {
		persist(vendor);
		
	}

	@Override
	public void deleteVendor(Vendor vendor) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void modifyVendor(Vendor vendor) {
		// TODO Auto-generated method stub
		
	}

	 @SuppressWarnings("unchecked")
		@Override
	    @Transactional
	public List<Vendor> listAllVendors() {
		Criteria criteria = getSession().createCriteria(Vendor.class);
        return (List<Vendor>) criteria.list();
	}

	 
	 @SuppressWarnings("unchecked")
		@Override
	    @Transactional
	public List<Vendor> listVendorsOnCrit(Vendor vendor) {
		 Criteria criteria = getSession().createCriteria(Vendor.class);
		 if(vendor.getName()!=null)
			 criteria.add(Restrictions.eq("name", vendor.getName()));
		 if(vendor.getId()!=null)
			 criteria.add(Restrictions.eq("id", vendor.getId()));
		 if(vendor.getEmail()!=null)
			 criteria.add(Restrictions.eq("email", vendor.getEmail()));
		 if(vendor.getPhoneNo()!=null)
			 criteria.add(Restrictions.eq("phoneNo", vendor.getPhoneNo()));
		 if(vendor.isBlockFlg()!=false)
			 criteria.add(Restrictions.eq("id", vendor.getId()));
		 if(vendor.getId()!=null)
			 criteria.add(Restrictions.eq("id", vendor.getId()));
		
     return (List<Vendor>) criteria.list();
	}

	

	/*@SuppressWarnings("unchecked")
	@Override
    @Transactional
    public List<Category> findAllCategory() {
        Criteria criteria = getSession().createCriteria(Category.class);
        return (List<Category>) criteria.list();
    }

	@Override
	public void saveCategory(Category category) {
		persist(category);	
	}

	@Override
	public void deleteCategory(Category category) {
		delete(category);	
		
	}*/
 
 /*   @SuppressWarnings("unchecked")
	@Override
    @Transactional
    public List<User> findAllUsers() {
        Criteria criteria = getSession().createCriteria(User.class);
        return (List<User>) criteria.list();
    }

	@Override
	public void saveUser(User user) {
		persist(user);	
	}*/
 
}
