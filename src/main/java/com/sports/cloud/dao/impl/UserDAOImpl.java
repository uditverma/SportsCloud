package main.java.com.sports.cloud.dao.impl;



import java.util.List;

import javax.transaction.Transactional;

import main.java.com.sports.cloud.dao.AbstractDao;
import main.java.com.sports.cloud.dao.UserDAO;
import main.java.com.sports.cloud.model.User;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;



@Repository("userDao")
public class UserDAOImpl extends AbstractDao implements UserDAO {
 
	
	public UserDAOImpl() {
		super();
	}
	
	public UserDAOImpl(SessionFactory sessionFactory) {
		super(sessionFactory);
	}
 
    @SuppressWarnings("unchecked")
	@Override
    @Transactional
    public List<User> ListAllUsers() {
        Criteria criteria = getSession().createCriteria(User.class);
        return (List<User>) criteria.list();
    }

	@Override
	public void saveUser(User user) {
		persist(user);	
	}

	@Override
	public void deleteUser(User user) {
		delete(user);
		
	}
 
}
