package main.java.com.sports.cloud.dao.impl;

import java.util.List;

import javax.transaction.Transactional;

import main.java.com.sports.cloud.dao.AbstractDao;
import main.java.com.sports.cloud.dao.FavClubDao;
import main.java.com.sports.cloud.model.FavClub;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;

public class FavClubDaoImpl extends AbstractDao implements FavClubDao {

	public FavClubDaoImpl() {
		super();
	}

	public FavClubDaoImpl(SessionFactory sessionFactory) {
		super(sessionFactory);
		
	}

	@SuppressWarnings("unchecked")
	@Override
    @Transactional
	public List<FavClub> listAllFavClub() {
		 Criteria criteria = getSession().createCriteria(FavClub.class);
	        return (List<FavClub>) criteria.list();
	}

	@Override
	public void createFavClub(FavClub favClub) {
		persist(favClub);
		
	}

	@Override
	public void deleteFavClub(FavClub favclub) {
		delete(favclub);	
		}
}
