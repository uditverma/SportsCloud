package main.java.com.sports.cloud.dao.impl;

import java.util.List;

import javax.transaction.Transactional;

import main.java.com.sports.cloud.dao.AbstractDao;
import main.java.com.sports.cloud.dao.TransactionDao;
import main.java.com.sports.cloud.model.Transaction;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;

public class TransactionDaoImpl extends AbstractDao implements TransactionDao {

	public TransactionDaoImpl() {
		super();
	}

	public TransactionDaoImpl(SessionFactory sessionFactory) {
		super(sessionFactory);

	}

	@SuppressWarnings("unchecked")
	@Override
    @Transactional
	public List<Transaction> listAllTransactions() {
		 Criteria criteria = getSession().createCriteria(Transaction.class);
	        return (List<Transaction>) criteria.list();	}

	@Override
	public void createTransaction(Transaction transaction) {
		persist(transaction);
		
	}

	@Override
	public void deleteTransaction(Transaction transaction) {
		delete(transaction);
		
	}

}
