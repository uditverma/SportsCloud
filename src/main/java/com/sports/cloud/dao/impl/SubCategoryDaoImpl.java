package main.java.com.sports.cloud.dao.impl;



import java.util.List;

import javax.transaction.Transactional;

import main.java.com.sports.cloud.config.SubCategoryFilter;
import main.java.com.sports.cloud.dao.AbstractDao;
import main.java.com.sports.cloud.dao.SubCategoryDao;
import main.java.com.sports.cloud.model.SubCategory;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;



@Repository("SubCategoryDao")
public class SubCategoryDaoImpl extends AbstractDao implements SubCategoryDao {
 
	
	public SubCategoryDaoImpl() {
		super();
	}
	
	public SubCategoryDaoImpl(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	@SuppressWarnings("unchecked")
	@Override
    @Transactional
    public List<SubCategory> ListAllSubCategory() {
        Criteria criteria = getSession().createCriteria(SubCategory.class);
        return (List<SubCategory>) criteria.list();
    }

	@Override
	public void saveSubCategory(SubCategory subCategory) {
		persist(subCategory);	
	}

	@Override
	public void deleteSubCategory(SubCategory subCategory) {
		delete(subCategory);	
		
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<SubCategory> findSubCategory(SubCategory subCategory, Enum<SubCategoryFilter> filter) {
		
		 Criteria criteria = getSession().createCriteria(SubCategory.class);
		 
		 if(filter==SubCategoryFilter.Name)
			 
			 criteria.add(Restrictions.like("name", subCategory.getName()));
		 
		 else if(filter==SubCategoryFilter.CategoryId)
			 {
			// System.out.println("categorySelected");
			 
			 criteria.add(Restrictions.like("categoryId", subCategory.getCategoryId()));
			 }
		 
		 else if(filter==SubCategoryFilter.Id)
			 criteria.add(Restrictions.like("id", subCategory.getId()));
		 
	        return (List<SubCategory>) criteria.list();
	}


	
	
	
 
 /*   @SuppressWarnings("unchecked")
	@Override
    @Transactional
    public List<User> findAllUsers() {
        Criteria criteria = getSession().createCriteria(User.class);
        return (List<User>) criteria.list();
    }

	@Override
	public void saveUser(User user) {
		persist(user);	
	}*/
 
}
