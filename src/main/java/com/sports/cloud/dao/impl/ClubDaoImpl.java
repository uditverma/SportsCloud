/**
 * 
 */
package main.java.com.sports.cloud.dao.impl;

import java.util.List;

import javax.transaction.Transactional;







import main.java.com.sports.cloud.dao.AbstractDao;
import main.java.com.sports.cloud.dao.ClubDao;
import main.java.com.sports.cloud.model.Club;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;

/**
 * @author DK
 *
 */
public class ClubDaoImpl extends AbstractDao implements ClubDao {

	public ClubDaoImpl() {
		super();
	}
	
	public ClubDaoImpl(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	
	
	@SuppressWarnings("unchecked")
	@Override
    @Transactional
	public List<Club> listAllClub() {
		 Criteria criteria = getSession().createCriteria(Club.class);
	        return (List<Club>) criteria.list();
	}

	

	@Override
	public void createClub(Club club) {
		persist(club);	}

	@Override
	public void deleteClub(Club club) {
		delete(club);
	}

	

}
