package main.java.com.sports.cloud.dao.impl;



import java.util.List;

import javax.transaction.Transactional;

import main.java.com.sports.cloud.dao.AbstractDao;
import main.java.com.sports.cloud.dao.CategoryDao;
import main.java.com.sports.cloud.model.Category;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;



@Repository("CategoryDao")
public class CategoryDaoImpl extends AbstractDao implements CategoryDao {
 
	
	public CategoryDaoImpl() {
		super();
	}
	
	public CategoryDaoImpl(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	@SuppressWarnings("unchecked")
	@Override
    @Transactional
    public List<Category> findAllCategory() {
        Criteria criteria = getSession().createCriteria(Category.class);
        return (List<Category>) criteria.list();
    }

	@Override
	public void saveCategory(Category category) {
		persist(category);	
	}

	@Override
	public void deleteCategory(Category category) {
		delete(category);	
		
	}
	

  
}
