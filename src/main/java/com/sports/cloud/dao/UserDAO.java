package main.java.com.sports.cloud.dao;



import java.util.List;

import main.java.com.sports.cloud.model.User;


 
public interface UserDAO {
    public List<User> ListAllUsers();
    public void saveUser(User user);
    public void deleteUser(User user);
}
