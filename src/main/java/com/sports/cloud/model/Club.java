/**
 * 
 */
package main.java.com.sports.cloud.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * @author dkukreja
 *
 */

@Entity
@Table(name = "Club", uniqueConstraints = {@UniqueConstraint(columnNames = {"name", "vendorId"})})
public class Club extends DomainObject{

	private static final long serialVersionUID = 2934135574182600218L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id",nullable = false, unique=true)
	private Long id;
	
	
	@Column(name = "name",nullable = false, length = 100)
	private String name;

	@Column(name = "remark",nullable = true, length = 400)
	private String remark;
	
	@Column(name = "email",nullable = false, length = 250)
	private String email;
	
	@Column(name = "phoneNo",nullable = false, length =10)
	private String phoneNo;
	
	@Column(name = "add1",nullable = false, length = 200)
	private String add1;
	
	@Column(name = "add2",nullable = true, length = 200)
	private String add2;
	
	@Column(name = "city",nullable = false, length = 100)
	private String city;
	
	@Column(name = "state",nullable = false, length = 100)
	private String state;
	
	@Column(name = "area",nullable = false, length = 100)
	private String area;
	
	@Column(name = "blockFlg",nullable = true)
	private boolean blockFlg;
	
	@Column(name = "actDate",nullable = false)
	private Date actDate;
	
	@Column(name = "lattitude",nullable = false)
	private Long lattitude;
	
	@Column(name = "longitude",nullable = false)
	private Long longitude;
	
	@Column(name = "pincode",nullable = false, length =10)
	private String pincode;
	
	@Column(name = "intRating",nullable = false)
	private int intRating;
	
	@Column(name = "extRating",nullable = false)
	private String extRating;
	
	@Column(name = "vendorId",nullable = false)
	private Long vendorId;
	
	/**
	 * @return the lattitude
	 */
	public Long getLattitude() {
		return lattitude;
	}

	/**
	 * @param lattitude the lattitude to set
	 */
	public void setLattitude(Long lattitude) {
		this.lattitude = lattitude;
	}

	/**
	 * @return the longitude
	 */
	public Long getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(Long longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the intRating
	 */
	public int getIntRating() {
		return intRating;
	}

	/**
	 * @param intRating the intRating to set
	 */
	public void setIntRating(int intRating) {
		this.intRating = intRating;
	}

	/**
	 * @return the extRating
	 */
	public String getExtRating() {
		return extRating;
	}

	/**
	 * @param extRating the extRating to set
	 */
	public void setExtRating(String extRating) {
		this.extRating = extRating;
	}

	/**
	 * @return the vendorId
	 */
	public Long getVendorId() {
		return vendorId;
	}

	/**
	 * @param vendorId the vendorId to set
	 */
	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the phoneNo
	 */
	public String getPhoneNo() {
		return phoneNo;
	}

	/**
	 * @param phoneNo the phoneNo to set
	 */
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	/**
	 * @return the add1
	 */
	public String getAdd1() {
		return add1;
	}

	/**
	 * @param add1 the add1 to set
	 */
	public void setAdd1(String add1) {
		this.add1 = add1;
	}

	/**
	 * @return the add2
	 */
	public String getAdd2() {
		return add2;
	}

	/**
	 * @param add2 the add2 to set
	 */
	public void setAdd2(String add2) {
		this.add2 = add2;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the area
	 */
	public String getArea() {
		return area;
	}

	/**
	 * @param area the area to set
	 */
	public void setArea(String area) {
		this.area = area;
	}

	/**
	 * @return the blockFlg
	 */
	public boolean isBlockFlg() {
		return blockFlg;
	}

	/**
	 * @param blockFlg the blockFlg to set
	 */
	public void setBlockFlg(boolean blockFlg) {
		this.blockFlg = blockFlg;
	}

	/**
	 * @return the actDate
	 */
	public Date getActDate() {
		return actDate;
	}

	/**
	 * @param actDate the actDate to set
	 */
	public void setActDate(Date actDate) {
		this.actDate = actDate;
	}

	/**
	 * @return the pincode
	 */
	public String getPincode() {
		return pincode;
	}

	/**
	 * @param pincode the pincode to set
	 */
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark =remark;
	}

}
