/**
 * 
 */
package main.java.com.sports.cloud.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * @author dkukreja
 *
 */

@Entity
@Table(name = "Activity", uniqueConstraints = {@UniqueConstraint(columnNames = {"vendorId","clubId","name"})})
public class Activity extends DomainObject{

	private static final long serialVersionUID = 2934135574182600219L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id",nullable = false)
	private Long id;
	

	@Column(name = "name",nullable = false, length = 100)
	private String name;

	@Column(name = "remark",nullable = true, length = 400)
	private String remark;
	
/*	@Column(name = "email",nullable = false, length = 250)
	private String email;
	
	@Column(name = "phoneNo",nullable = false, length =10)
	private String phoneNo;
	
	@Column(name = "add1",nullable = false, length = 200)
	private String add1;
	
	@Column(name = "add2",nullable = true, length = 200)
	private String add2;
	
	@Column(name = "city",nullable = false, length = 100)
	private String city;
	
	@Column(name = "state",nullable = false, length = 100)
	private String state;
	
	@Column(name = "area",nullable = false, length = 100)
	private String area;*/
	
	@Column(name = "blockFlg",nullable = true)
	private int blockFlg;
	
	/*@Column(name = "actDate",nullable = false)
	private Date actDate;
	
	@Column(name = "pincode",nullable = false, length =10)
	private String pincode;*/
	
	@Column(name = "vendorId",nullable = false)
	private Long vendorId;

	@Column(name = "clubId",nullable = false)
	private Long clubId;
	
	
	@Column(name = "oTime",nullable = false)
	private Date oTime;

	@Column(name = "cTime",nullable = false)
	private Date cTime;
	
	
	@Column(name = "pnpMode",nullable = true)
	private int pnpMode;
	
	@Column(name = "subMode",nullable = true)
	private int subMode;
	
	@Column(name = "pnpId",nullable = true)
	private Long pnpId;
	
	@Column(name = "subsId",nullable = true)
	private Long subsId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public int getBlockFlg() {
		return blockFlg;
	}

	public void setBlockFlg(int blockFlg) {
		this.blockFlg = blockFlg;
	}

	public Long getVendorId() {
		return vendorId;
	}

	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}

	public Long getClubId() {
		return clubId;
	}

	public void setClubId(Long clubId) {
		this.clubId = clubId;
	}

	public Date getoTime() {
		return oTime;
	}

	public void setoTime(Date oTime) {
		this.oTime = oTime;
	}

	public Date getcTime() {
		return cTime;
	}

	public void setcTime(Date cTime) {
		this.cTime = cTime;
	}



	public int getPnpMode() {
		return pnpMode;
	}

	public void setPnpMode(int pnpMode) {
		this.pnpMode = pnpMode;
	}

	public int getSubMode() {
		return subMode;
	}

	public void setSubMode(int subMode) {
		this.subMode = subMode;
	}

	public Long getPnpId() {
		return pnpId;
	}

	public void setPnpId(Long pnpId) {
		this.pnpId = pnpId;
	}

	public Long getSubsId() {
		return subsId;
	}

	public void setSubsId(Long subsId) {
		this.subsId = subsId;
	}
	

	
	
}
