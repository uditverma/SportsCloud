/**
 * 
 */
package main.java.com.sports.cloud.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * @author dkukreja
 *
 */

@Entity
@Table(name = "FavClub", uniqueConstraints = {@UniqueConstraint(columnNames = {"userId","activityId"})})
public class FavClub extends DomainObject{
	
	private static final long serialVersionUID = 2934135574182600222L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "userId", nullable=false)
	private Long userId;
	
	/*@Column(name = "clubId", nullable=false)
	private Long clubId;*/
	
	@Column(name = "activityId", nullable=false)
	private Long activityId;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the clubId
	 */
	/*public Long getClubId() {
		return clubId;
	}

	*//**
	 * @param clubId the clubId to set
	 *//*
	public void setClubId(Long clubId) {
		this.clubId = clubId;
	}*/

	/**
	 * @return the sportsId
	 */
	public Long getActivityId() {
		return activityId;
	}

	/**
	 * @param sportsId the sportsId to set
	 */
	public void setActivityId(Long activityId) {
		this.activityId = activityId;
	}

}
