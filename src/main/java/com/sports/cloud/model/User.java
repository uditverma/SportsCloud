package main.java.com.sports.cloud.model;



import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "User")
public class User extends DomainObject{

	private static final long serialVersionUID = 2934135574182600214L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	

	@Column(name = "username",nullable = false, length =30)
	private String username;
	
	@Column(name = "password",nullable = false, length=20)
	private String password;
	

	@Column(name = "email",nullable = false, unique=true,length=400)
	private String email;
	

	@Column(name = "mobNum",nullable = false, unique=true,length=10)
	private Number mobNum;
	
	@Column(name = "verifyNum")
	private int verifyNum;
	
	@Column(name = "name",nullable = false, length=20)
	private String name;
	
	@Column(name = "surname",nullable = false, length=40)
	private String surname;
	
	@Column(name = "loginType",nullable = false)
	private int loginType;

	@Column(name = "suspendflg")
	private int suspendflg;
	
	@Column(name = "actDate")
	private Date actDate;
	
	@Column(name = "dob")
	private Date dob;
	
	
	@Column(name = "loginSrc")
	private int loginSrc;
	
	
	/**
	 * @return the mobNum
	 */
	public Number getMobNum() {
		return mobNum;
	}

	/**
	 * @param mobNum the mobNum to set
	 */
	public void setMobNum(Number mobNum) {
		this.mobNum = mobNum;
	}

	/**
	 * @return the verifyNum
	 */
	public int getVerifyNum() {
		return verifyNum;
	}

	/**
	 * @param verifyNum the verifyNum to set
	 */
	public void setVerifyNum(int verifyNum) {
		this.verifyNum = verifyNum;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * @param surname the surname to set
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}

	/**
	 * @return the loginType
	 */
	public int getLoginType() {
		return loginType;
	}

	/**
	 * @param loginType the loginType to set
	 */
	public void setLoginType(int loginType) {
		this.loginType = loginType;
	}

	/**
	 * @return the suspendflg
	 */
	public int getSuspendflg() {
		return suspendflg;
	}

	/**
	 * @param suspendflg the suspendflg to set
	 */
	public void setSuspendflg(int suspendflg) {
		this.suspendflg = suspendflg;
	}

	/**
	 * @return the actDate
	 */
	public Date getActDate() {
		return actDate;
	}

	/**
	 * @param actDate the actDate to set
	 */
	public void setActDate(Date actDate) {
		this.actDate = actDate;
	}

	/**
	 * @return the dob
	 */
	public Date getDob() {
		return dob;
	}

	/**
	 * @param dob the dob to set
	 */
	public void setDob(Date dob) {
		this.dob = dob;
	}

	/**
	 * @return the loginSrc
	 */
	public int getLoginSrc() {
		return loginSrc;
	}

	/**
	 * @param loginSrc the loginSrc to set
	 */
	public void setLoginSrc(int loginSrc) {
		this.loginSrc = loginSrc;
	}

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
