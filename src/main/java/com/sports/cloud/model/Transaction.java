/**
 * 
 */
package main.java.com.sports.cloud.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author dkukreja
 *
 */

@Entity
@Table(name = "Transaction")
public class Transaction extends DomainObject{
	
	private static final long serialVersionUID = 2934135574182600221L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "userId", nullable=false)
	private Long userId;
	
	@Column(name = "clubId", nullable=false)
	private Long clubId;
	
	@Column(name = "activityId", nullable=false)
	private Long activityId;
	
	@Column(name = "payType", nullable=false)
	private int payType;
	
	@Column(name = "amt", nullable=false)
	private int amt;
	
	@Column(name = "bookDate", nullable=false)
	private Date bookDate;

	@Column(name = "bookType", nullable=false)
	private int bookType;
	
	@Column(name = "memNum", nullable=false, length=20)
	private String memNum;
	
	/**
	 * @return the bookType
	 */
	public int getBookType() {
		return bookType;
	}

	/**
	 * @param bookType the bookType to set
	 */
	public void setBookType(int bookType) {
		this.bookType = bookType;
	}

	/**
	 * @return the memNum
	 */
	public String getMemNum() {
		return memNum;
	}

	/**
	 * @param memNum the memNum to set
	 */
	public void setMemNum(String memNum) {
		this.memNum = memNum;
	}


	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the clubId
	 */
	public Long getClubId() {
		return clubId;
	}

	/**
	 * @param clubId the clubId to set
	 */
	public void setClubId(Long clubId) {
		this.clubId = clubId;
	}

	/**
	 * @return the sportsId
	 */
	public Long getActivityId() {
		return activityId;
	}

	/**
	 * @param sportsId the sportsId to set
	 */
	public void setActivityId(Long activityId) {
		this.activityId = activityId;
	}

	/**
	 * @return the payType
	 */
	public int getPayType() {
		return payType;
	}

	/**
	 * @param payType the payType to set
	 */
	public void setPayType(int payType) {
		this.payType = payType;
	}

	/**
	 * @return the amt
	 */
	public int getAmt() {
		return amt;
	}

	/**
	 * @param amt the amt to set
	 */
	public void setAmt(int amt) {
		this.amt = amt;
	}

	/**
	 * @return the bookDate
	 */
	public Date getBookDate() {
		return bookDate;
	}

	/**
	 * @param bookDate the bookDate to set
	 */
	public void setBookDate(Date bookDate) {
		this.bookDate = bookDate;
	}


}