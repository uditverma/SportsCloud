package main.java.com.sports.cloud.model;



import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ErrorMessage extends DomainObject{

	private static final long serialVersionUID = 2934135574182600211L;
	
    private List<String> errors;

    public ErrorMessage() {
    }

    public ErrorMessage(List<String> errors) {
        this.errors = errors;
    }

    public ErrorMessage(String error) {
        this(Collections.singletonList(error));
    }

    public ErrorMessage(String ... errors) {
        this(Arrays.asList(errors));
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }
}