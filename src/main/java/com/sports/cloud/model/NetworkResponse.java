package main.java.com.sports.cloud.model;



import java.io.Serializable;

public class NetworkResponse implements Serializable{

	private static final long serialVersionUID = 1822404527803701966L;
	
	private boolean failure;
	private String desc;
	private DomainObject obj;
	
	public boolean isFailure() {
		return failure;
	}
	public void setFailure(boolean failure) {
		this.failure = failure;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public DomainObject getObj() {
		return obj;
	}
	public void setObj(DomainObject obj) {
		this.obj = obj;
	}
}
