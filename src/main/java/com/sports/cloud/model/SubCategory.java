package main.java.com.sports.cloud.model;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "SubCategory", uniqueConstraints = {@UniqueConstraint(columnNames = {"name", "categoryId"})})
public class SubCategory extends DomainObject{

	private static final long serialVersionUID = 2934135574182600216L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id",nullable = false, unique=true)
	private Long id;
	
	@Column(name = "name",nullable = false)
	private String name;

	@Column(name = "remark",nullable = true)
	private String remark;
	
	@Column(name = "categoryId",nullable = false)
	private Long categoryId;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark =remark;
	}
	
	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

}
