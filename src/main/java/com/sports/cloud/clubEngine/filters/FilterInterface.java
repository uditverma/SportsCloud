package main.java.com.sports.cloud.clubEngine.filters;

import java.util.LinkedList;

import main.java.com.sports.cloud.clubEngine.Criteria;
import main.java.com.sports.cloud.model.Activity;

public interface FilterInterface {

	//public LinkedList<String> initializeFilterData();
	public LinkedList<Activity> executeFilterData(Criteria filterCrit);

	//LinkedList<String> executeFilterData();
}
