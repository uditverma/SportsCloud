package main.java.com.sports.cloud.clubEngine.filters;

import java.util.LinkedList;

import org.springframework.beans.factory.annotation.Autowired;

import main.java.com.sports.cloud.clubEngine.Criteria;
import main.java.com.sports.cloud.model.Activity;
import main.java.com.sports.cloud.service.ActivityService;
import main.java.com.sports.cloud.service.impl.ActivityServiceImpl;


public class ActivityFilter implements FilterInterface {

	@Autowired
	ActivityService activityService=new ActivityServiceImpl();
	
	public ActivityFilter() {
		// TODO Auto-generated constructor stub
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public LinkedList executeFilterData(Criteria filterCrit) {
		// Applying Sports Filter
		
		
		
		Activity tempFilter=new Activity();
		tempFilter.setName(filterCrit.getActivityName());
		/*
		 * 0=pnp
		 * 1=subs
		 */
		if(filterCrit.getClubType()==0)
			tempFilter.setPnpMode(1);
		if(filterCrit.getClubType()==1)
			tempFilter.setSubMode(1);
		
		
		LinkedList<Activity> FilteredActivityList=(LinkedList<Activity>) activityService.FetchActivity(tempFilter);
		return FilteredActivityList;
		
	}

	

}
