package main.java.com.sports.cloud.clubEngine.sorters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;




public class LocationSort {

		public static TreeMap<Integer,String> getClubDistance(LinkedHashMap<Long, String> clubLocationMAP,String currentLocation,String key)
		{
			//Iterator<Long> itrLoc=clubLocationMAP.keySet().iterator();
			URL url;
			TreeMap<Integer,String> clubDistanceMap=new TreeMap<Integer,String>();
			LinkedList<Long> clubIdList=new LinkedList<Long>();
			HttpURLConnection conn;
			String line, distanceAPIResp = "";
			InputStreamReader x=null;
			Parent objPar=null;
			//Set temp=clubDistanceMap.keySet();
			
			//Create URL
			StringBuffer urlStringBuffer=new StringBuffer("https://maps.googleapis.com/maps/api/distancematrix/json?origins=");
			urlStringBuffer.append(currentLocation);
			urlStringBuffer.append("&destinations=");
			int i=0;
			for(Entry<Long, String> entry: clubLocationMAP.entrySet())
			{
				clubIdList.add(entry.getKey());
				urlStringBuffer.append(entry.getValue());
				if(i<clubLocationMAP.size()-1)
					urlStringBuffer.append("|");
				i++;
			}
			urlStringBuffer.append("&key=");
			urlStringBuffer.append(key);

			
			System.out.println(urlStringBuffer.toString());
			try 
			{
				url = new URL(urlStringBuffer.toString());
				conn = (HttpURLConnection) url.openConnection();
				x=new InputStreamReader(conn.getInputStream());
				BufferedReader reader = new BufferedReader(x);
				while ((line = reader.readLine()) != null) 
				{
					distanceAPIResp += line;
				}
			}
			catch (MalformedURLException e) 
			{
				
				e.printStackTrace();
			} catch (IOException e)
			{
				
				e.printStackTrace();
			}

			//conn.setRequestMethod("GET");
			

			try 
			{
				objPar = new ObjectMapper().readValue(distanceAPIResp,Parent.class);
			} 
			catch (JsonParseException e) 
			{
				e.printStackTrace();
			} 
			catch (JsonMappingException e) 
			{
				e.printStackTrace();
			}
			catch (IOException e) 
			{
				e.printStackTrace();
			}
			
			//Row[] r=objPar.getRows();
			if(objPar.getStatus().compareToIgnoreCase("OK")==0)
			{	//System.out.println(objPar.getStatus());
				for(int j=0;j<objPar.getRows().length;j++)
				{
					//Element[] e=objPar.getRows()[i].elements;
					for(int k=0;k<objPar.getRows()[j].elements.length;k++)
					{
						//sSystem.out.println(objPar.getDestination_addresses()[k].toString());
						if(objPar.getRows()[j].elements[k].status.compareToIgnoreCase("OK")==0)
							clubDistanceMap.put(objPar.getRows()[j].elements[k].distance.value,clubIdList.get(k)+"|"+objPar.getRows()[j].elements[k].distance.text);
					}
				}
			}
			else
				return null;
			return clubDistanceMap;
		}
		
		public static void main(String[] args) 
		{
			String origin="28.4456124,77.03330729999993";
			LinkedHashMap<Long, String> x=new LinkedHashMap<Long, String>();
			x.put((long) 1, "28.4612568,77.02981920000002");
			x.put((long) 3, "28.4620498,77.00178619999997");
			x.put((long) 2, "28.46574279999999,77.01686339999992");
			System.out.println(getClubDistance(x,origin,"AIzaSyCBCUp6LtDVl0DTsG4Q_7bwq4mfbj5RYB0"));
			
		
			


				 
				
		}

	

}
