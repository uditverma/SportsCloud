package main.java.com.sports.cloud.clubEngine.sorters;



public class Row
{
    public Element[] elements;

	/**
	 * @return the elements
	 */
	public Element[] getElements() {
		return elements;
	}

	/**
	 * @param elements the elements to set
	 */
	public void setElements(Element[] elements) {
		this.elements = elements;
	}
}