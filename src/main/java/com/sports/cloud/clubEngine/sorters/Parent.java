package main.java.com.sports.cloud.clubEngine.sorters;



public class Parent
{
    public String[] destination_addresses;
    /**
	 * @return the destination_addresses
	 */
	public String[] getDestination_addresses() {
		return destination_addresses;
	}
	/**
	 * @param destination_addresses the destination_addresses to set
	 */
	public void setDestination_addresses(String[] destination_addresses) {
		this.destination_addresses = destination_addresses;
	}
	/**
	 * @return the origin_addresses
	 */
	public String[] getOrigin_addresses() {
		return origin_addresses;
	}
	/**
	 * @param origin_addresses the origin_addresses to set
	 */
	public void setOrigin_addresses(String[] origin_addresses) {
		this.origin_addresses = origin_addresses;
	}
	/**
	 * @return the rows
	 */
	public Row[] getRows() {
		return rows;
	}
	/**
	 * @param rows the rows to set
	 */
	public void setRows(Row[] rows) {
		this.rows = rows;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	public String[] origin_addresses;
    public Row[] rows;
    public String status;
}