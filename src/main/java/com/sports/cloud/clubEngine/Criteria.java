package main.java.com.sports.cloud.clubEngine;

import java.util.Date;

public class Criteria {

	private Date ReqBookingDate=new Date();
	private Date ReqTimeStart=new Date();
	private Date ReqTimeEnd=new Date();
	private int clubFlg;
	private int clubType;
	private String ActivityName;
	public Date getReqBookingDate() {
		return ReqBookingDate;
	}
	public void setReqBookingDate(Date reqBookingDate) {
		ReqBookingDate = reqBookingDate;
	}
	public Date getReqTimeStart() {
		return ReqTimeStart;
	}
	public void setReqTimeStart(Date reqTimeStart) {
		ReqTimeStart = reqTimeStart;
	}
	public Date getReqTimeEnd() {
		return ReqTimeEnd;
	}
	public void setReqTimeEnd(Date reqTimeEnd) {
		ReqTimeEnd = reqTimeEnd;
	}
	public int getClubFlg() {
		return clubFlg;
	}
	public void setClubFlg(int clubFlg) {
		this.clubFlg = clubFlg;
	}
	public int getClubType() {
		return clubType;
	}
	public void setClubType(int clubType) {
		this.clubType = clubType;
	}
	
	public Criteria(Date reqBookingDate, Date reqTimeStart, Date reqTimeEnd,
			int clubFlg, int clubType, String activity) {
		super();
		this.ReqBookingDate = reqBookingDate;
		this.ReqTimeStart = reqTimeStart;
		this.ReqTimeEnd = reqTimeEnd;
		this.clubFlg = clubFlg;
		this.clubType = clubType;
		this.ActivityName = activity;
	}
	public String getActivityName() {
		return ActivityName;
	}
	public void setActivityName(String activityName) {
		ActivityName = activityName;
	}
	
	


}
